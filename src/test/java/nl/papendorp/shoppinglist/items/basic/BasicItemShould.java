package nl.papendorp.shoppinglist.items.basic;

import nl.papendorp.shoppinglist.items.Item;
import nl.papendorp.shoppinglist.items.ItemFactory;
import org.junit.Before;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.function.Supplier;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

class BasicItemShould
{
	private static final String PRODUCT_NAME = "PRODUCT_NAME";
	private static final int QUANTITY = 1;

	private ItemFactory factory;

	@BeforeEach
	void withFactory()
	{
		factory = new BasicItemFactory();
	}

	@Test
	void haveNameAndQuantity()
	{
		Item item = itemFor( PRODUCT_NAME, QUANTITY );

		assertThat( item.productName(), is( PRODUCT_NAME ) );
		assertThat( item.quantity(), is( QUANTITY ) );
	}

	@Test
	void beEqual_forEqualFields()
	{
		Item item = itemFor( PRODUCT_NAME, QUANTITY );

		assertThat( item, is( itemFor( PRODUCT_NAME, QUANTITY ) ) );

		assertThat( item, is( not( itemFor( "OTHER_PRODUCT_NAME", QUANTITY ) ) ) );
		assertThat( item, is( not( itemFor( PRODUCT_NAME, 2 ) ) ) );
	}

	@Test
	void ensureValidQuantity()
	{
		expectException( "Illegal quantity: 0", () -> itemFor( PRODUCT_NAME, 0 ) );
		expectException( "Illegal quantity: -1", () -> itemFor( PRODUCT_NAME, -1 ) );
	}

	@Test
	void ensureValidProductName()
	{
		expectException( "Missing product name", () -> itemFor( "", 1 ) );
		expectException( "Missing product name", () -> itemFor( "   ", 1 ) );
		expectException( "Missing product name", () -> itemFor( null, 1 ) );
	}

	@Test
	void addQuantities()
	{
		Item item = itemFor( PRODUCT_NAME, 2 );
		Item added = item.addQuantity( 3 );

		assertThat( added.quantity(), is( 5 ) );
	}

	@Test
	void prettyPrint()
	{
		Item item = itemFor( PRODUCT_NAME, 2 );

		assertThat( item.toString(), is( "BasicItem(product=Product(name=PRODUCT_NAME), quantity=Quantity(value=2))" ) );
	}

	private Item itemFor( String productName, int quantity )
	{
		return factory.buildItem()
				.withProductName( productName )
				.withQuantity( quantity );
	}

	private void expectException( String expectedErrorMessage, Supplier< Item > block )
	{
		try
		{
			Item result = block.get();
			fail( "Expected exception, but result was: " + result );
		}
		catch( IllegalArgumentException iae )
		{
			assertThat( iae.getMessage(), is( expectedErrorMessage ) );
		}
	}
}