package nl.papendorp.shoppinglist.items;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.Map;

@AllArgsConstructor
@EqualsAndHashCode
@ToString
public class SimpleItem implements Item
{
	private final String productName;
	private final Integer quantity;

	public SimpleItem( Item other )
	{
		this( other.productName(), other.quantity() );
	}

	public SimpleItem( Map< String, String > entry )
	{
		this( entry.get( "productName" ),
				Integer.parseInt( entry.get( "quantity" ) ) );
	}

	@Override
	public String productName()
	{
		return productName;
	}

	@Override
	public int quantity()
	{
		return quantity;
	}

	@Override
	public Item addQuantity( int addend )
	{
		return new SimpleItem( productName, quantity + addend );
	}
}
