package nl.papendorp.shoppinglist.features.addingLists;

import cucumber.api.java8.En;
import nl.papendorp.shoppinglist.configuration.ShoppingListSpringConfiguration;
import nl.papendorp.shoppinglist.features.configuration.AdditionalCucumberSpringConfiguration;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;

@ContextConfiguration( classes = { ShoppingListSpringConfiguration.class, AdditionalCucumberSpringConfiguration.class } )
@ActiveProfiles( "Cucumber" )
public class AddingListsStepDefinitions implements En
{
	public AddingListsStepDefinitions()
	{
		Before( this::removeAllLists );

		Given( "^that I have a list called (.+)$", this::withListCalled);
	}

	private void removeAllLists()
	{

	}

	private void withListCalled( String listName )
	{

	}
}
