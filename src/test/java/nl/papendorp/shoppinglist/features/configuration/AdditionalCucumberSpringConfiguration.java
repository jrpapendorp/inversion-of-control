package nl.papendorp.shoppinglist.features.configuration;

import nl.papendorp.shoppinglist.features.repository.CucumberRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;

@Configuration
@Profile( "Cucumber" )
public class AdditionalCucumberSpringConfiguration
{
	private final CucumberRepository mockShoppingListRepository;

	public AdditionalCucumberSpringConfiguration()
	{
		mockShoppingListRepository = new CucumberRepository();
	}

	@Primary
	@Bean
	@Lazy
	public CucumberRepository mockShoppingListRepository()
	{
		return mockShoppingListRepository;
	}
}
