package nl.papendorp.shoppinglist.features.repository;

import cucumber.api.DataTable;
import cucumber.api.java8.En;
import nl.papendorp.shoppinglist.configuration.ShoppingListSpringConfiguration;
import nl.papendorp.shoppinglist.features.configuration.AdditionalCucumberSpringConfiguration;
import nl.papendorp.shoppinglist.features.shared.CucumberItemConversion;
import nl.papendorp.shoppinglist.items.Item;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;

import javax.inject.Inject;
import java.util.Collection;

import static java.util.Collections.emptySet;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.junit.Assert.assertThat;

@ContextConfiguration( classes = { ShoppingListSpringConfiguration.class, AdditionalCucumberSpringConfiguration.class } )
@ActiveProfiles( "Cucumber" )
public class RepositoryStepDefinition implements En
{
	private final CucumberItemConversion converter = new CucumberItemConversion();
	private final CucumberRepository mockRepository;

	@Inject
	public RepositoryStepDefinition( CucumberRepository mockRepository )
	{
		this.mockRepository = mockRepository;

		Before( () -> mockRepository.updateList( emptySet() ) );

		Given( "^that my (.+) list contains$", this::withItemsOnList );

		Then( "^my (.+) list contains$", this::assertItemsOnList );
	}

	private void withItemsOnList( String listName, DataTable dataTable )
	{
		mockRepository.updateList( converter.toSimpleItems( dataTable ) );
	}

	private void assertItemsOnList( String listName, DataTable dataTable )
	{
		Collection< Item > actual = converter.toSimpleItems( mockRepository.allItemsOnMyList() );

		assertThat( actual, containsInAnyOrder( converter.toSimpleItems( dataTable ).toArray() ) );
	}
}
