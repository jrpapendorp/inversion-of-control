package nl.papendorp.shoppinglist.features.repository;

import nl.papendorp.shoppinglist.items.Item;
import nl.papendorp.shoppinglist.shoppinglist.stored.ShoppingListRepository;

import java.util.Collection;

import static com.google.common.collect.ImmutableSet.copyOf;
import static java.util.Collections.emptySet;

public class CucumberRepository implements ShoppingListRepository
{
	private Collection< Item > items = emptySet();

	@Override
	public Collection< Item > allItemsOnMyList()
	{
		return copyOf( items );
	}

	@Override
	public void updateList( Collection< Item > updatedList )
	{
		items = copyOf( updatedList );
	}
}
