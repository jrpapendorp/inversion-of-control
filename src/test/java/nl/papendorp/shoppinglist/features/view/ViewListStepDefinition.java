package nl.papendorp.shoppinglist.features.view;

import cucumber.api.DataTable;
import cucumber.api.java8.En;
import nl.papendorp.shoppinglist.configuration.ShoppingListSpringConfiguration;
import nl.papendorp.shoppinglist.features.configuration.AdditionalCucumberSpringConfiguration;
import nl.papendorp.shoppinglist.features.shared.CucumberItemConversion;
import nl.papendorp.shoppinglist.features.shared.attempt.Attempt;
import nl.papendorp.shoppinglist.items.Item;
import nl.papendorp.shoppinglist.viewing.ListViewingService;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;

import javax.inject.Inject;
import java.util.Collection;

import static nl.papendorp.shoppinglist.features.shared.attempt.Attempt.failure;
import static nl.papendorp.shoppinglist.features.shared.attempt.Attempt.resultOf;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.junit.Assert.assertThat;

@ContextConfiguration( classes = { ShoppingListSpringConfiguration.class, AdditionalCucumberSpringConfiguration.class } )
@ActiveProfiles( "Cucumber" )
public class ViewListStepDefinition implements En
{
	private final CucumberItemConversion converter = new CucumberItemConversion();
	private final ListViewingService listViewingService;

	private Attempt< Collection< Item > > actualList;

	@Inject
	public ViewListStepDefinition( ListViewingService listViewingService )
	{
		this.listViewingService = listViewingService;

		Before( this::resetRetrievedResult );

		When( "^I look at my (.+) list$", this::whenRetrievingShoppingList );

		Then( "^I see a list containing$", this::assertListWithItems );
	}

	private void resetRetrievedResult()
	{
		actualList = failure( new IllegalStateException( "List not yet retrieved." ) );
	}

	private void whenRetrievingShoppingList( String listName )
	{
		actualList = resultOf( () -> converter.toSimpleItems( listViewingService.viewMyListNamed( listName ) ) );
	}

	private void assertListWithItems( DataTable dataTable )
	{
		Collection< Item > items = converter.toSimpleItems( dataTable );

		assertThat( actualList.get(), containsInAnyOrder( items.toArray() ) );
	}
}
