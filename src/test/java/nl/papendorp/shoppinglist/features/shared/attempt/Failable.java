package nl.papendorp.shoppinglist.features.shared.attempt;

public interface Failable< RESULT >
{
	RESULT get() throws Exception;
}
