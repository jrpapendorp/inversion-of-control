package nl.papendorp.shoppinglist.features.shared;

import cucumber.api.DataTable;
import nl.papendorp.shoppinglist.items.Item;
import nl.papendorp.shoppinglist.items.SimpleItem;

import java.util.Collection;

import static java.util.stream.Collectors.toSet;

public class CucumberItemConversion
{
	public Collection< Item > toSimpleItems( DataTable dataTable )
	{
		return dataTable.asMaps( String.class, String.class )
				.stream()
				.map( SimpleItem::new )
				.collect( toSet() );
	}

	public Collection< Item > toSimpleItems( Collection< Item > items )
	{
		return items
				.stream()
				.map( SimpleItem::new )
				.collect( toSet() );
	}
}
