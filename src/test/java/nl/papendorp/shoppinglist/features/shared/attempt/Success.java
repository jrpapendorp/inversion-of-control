package nl.papendorp.shoppinglist.features.shared.attempt;

class Success< RESULT > implements Attempt< RESULT >
{
	private final RESULT result;

	Success( RESULT result )
	{
		this.result = result;
	}

	@Override
	public boolean succeeded()
	{
		return true;
	}

	@Override
	public RESULT get()
	{
		return result;
	}

	@Override
	public RuntimeException error()
	{
		throw new IllegalStateException( "Success has no error. Result was " + result );
	}
}
