package nl.papendorp.shoppinglist.features.shared.attempt;

class Failure<RESULT> implements Attempt<RESULT>
{
	private final Exception exception;

	Failure( Exception exception )
	{
		this.exception = exception;
	}

	@Override
	public boolean succeeded()
	{
		return false;
	}

	@Override
	public RESULT get()
	{
		throw new IllegalStateException( "Failure has no result.", exception );
	}

	@Override
	public Exception error()
	{
		return exception;
	}
}
