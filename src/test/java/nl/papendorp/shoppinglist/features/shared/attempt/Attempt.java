package nl.papendorp.shoppinglist.features.shared.attempt;

import java.util.function.Supplier;

public interface Attempt< RESULT > extends Supplier< RESULT >
{
	static < RESULT > Attempt< RESULT > success( RESULT result )
	{
		return new Success<>( result );
	}

	static < RESULT > Attempt< RESULT > failure( Exception exception )
	{
		return new Failure<>( exception );
	}

	static < RESULT > Attempt< RESULT > resultOf( Failable< RESULT > block )
	{
		try
		{
			return new Success<>( block.get() );
		}
		catch( Exception error )
		{
			return new Failure<>( error );
		}
	}

	boolean succeeded();

	default boolean failed()
	{
		return !succeeded();
	}

	RESULT get();

	Exception error();
}
