package nl.papendorp.shoppinglist.features;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;
import org.springframework.context.annotation.Profile;

@RunWith( Cucumber.class )
@Profile( "Cucumber" )
@CucumberOptions(
		monochrome = true,
		format = { "pretty" },
		glue = { "classpath:nl.papendorp.shoppinglist.features" },
		features = { "classpath:nl/papendorp/shoppinglist/features/" } )
public class CucumberTest
{
}
