package nl.papendorp.shoppinglist.features.addingItems;

import cucumber.api.java8.En;
import nl.papendorp.shoppinglist.configuration.ShoppingListSpringConfiguration;
import nl.papendorp.shoppinglist.features.configuration.AdditionalCucumberSpringConfiguration;
import nl.papendorp.shoppinglist.features.shared.attempt.Attempt;
import nl.papendorp.shoppinglist.items.SimpleItem;
import nl.papendorp.shoppinglist.viewing.ListViewingService;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;

import javax.inject.Inject;

import static nl.papendorp.shoppinglist.features.shared.attempt.Attempt.failure;
import static nl.papendorp.shoppinglist.features.shared.attempt.Attempt.resultOf;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.is;

@ContextConfiguration( classes = { ShoppingListSpringConfiguration.class, AdditionalCucumberSpringConfiguration.class } )
@ActiveProfiles( "Cucumber" )
public class AddingItemsStepDefinition implements En
{
	private final ListViewingService listViewingService;

	private Attempt< Void > result;

	@Inject
	public AddingItemsStepDefinition( ListViewingService listViewingService )
	{
		this.listViewingService = listViewingService;

		Before( this::resetResult );

		When( "I add (-?\\d+) items of (.+) to my (.+) list", this::whenAdding );

		Then( "^an (.+) error occurs$", this::assertError );
	}

	private void resetResult()
	{
		result = failure( new IllegalStateException( "Items not yet added." ) );
	}

	private void whenAdding( Integer quantity, String productName, String listName )
	{
		result = resultOf( () ->
		{
			listViewingService.addItemToMyListNamed( listName, new SimpleItem( productName, quantity ) );
			return null;
		} );
	}

	private void assertError( String error )
	{
		Exception actual = result.error();

		switch( error )
		{
			case "invalid quantity":
				assertThat( actual, is( instanceOf( IllegalArgumentException.class ) ) );
				assertThat( actual.getMessage(), containsString( "Illegal quantity: " ) );
				break;

			case "invalid product name":
				assertThat( actual, is( instanceOf( IllegalArgumentException.class ) ) );
				assertThat( actual.getMessage(), is( "Missing product name" ) );
				break;

			default:
				throw new IllegalStateException( "Unknown assertion: error type " + error );
		}
	}
}
