package nl.papendorp.shoppinglist.viewing;

import nl.papendorp.shoppinglist.items.Item;
import nl.papendorp.shoppinglist.items.ItemConverter;
import nl.papendorp.shoppinglist.items.SimpleItem;
import nl.papendorp.shoppinglist.shoppinglist.ShoppingList;
import nl.papendorp.shoppinglist.shoppinglist.ShoppingListFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Collection;

import static com.google.common.collect.ImmutableList.copyOf;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class ListViewingFacadeShould
{
	private static final String LIST_NAME = "LIST_NAME";

	@Mock
	private ShoppingListFactory shoppingListFactory;

	@Mock
	private ShoppingList mockShoppingList;

	private ListViewingService listViewingService;

	@BeforeEach
	void withShoppingList()
	{
		MockitoAnnotations.initMocks( this );

		when( shoppingListFactory.myListNamed( LIST_NAME ) )
				.thenReturn( mockShoppingList );
	}

	@BeforeEach
	void withListViewingService()
	{
		listViewingService = new ListViewingFacade( shoppingListFactory, ( item ) -> item );
	}

	@Test
	void retrieveMyShoppingList()
	{
		Item one = mock( Item.class );
		Item two = mock( Item.class );
		Item three = mock( Item.class );

		withItemsOnMyList( one, two, three );

		Collection< Item > actualItems = listViewingService.viewMyListNamed( LIST_NAME );

		assertThat( actualItems, contains( one, two, three ) );
	}

	@Test
	void addAnItemToMyList()
	{
		Item expected = new SimpleItem( "PRODUCT_NAME", 1 );

		listViewingService.addItemToMyListNamed( LIST_NAME, expected );

		verify( mockShoppingList ).addNewItem( expected );
	}

	@Test
	void propagateConversionErrors()
	{
		IllegalArgumentException expected = new IllegalArgumentException( "" );

		ListViewingFacade listViewingFacade = new ListViewingFacade( shoppingListFactory, converterThrowing( expected ) );

		try
		{
			listViewingFacade.addItemToMyListNamed( LIST_NAME, mock( Item.class ) );
			fail( "Excepted exception" );
		}
		catch( IllegalArgumentException actual )
		{
			assertThat( actual, is( expected ) );
		}
	}

	private ItemConverter converterThrowing( RuntimeException exception )
	{
		return ( item ) ->
		{
			throw exception;
		};
	}

	private void withItemsOnMyList( Item... items )
	{
		when( mockShoppingList.allItems() )
				.thenReturn( copyOf( items ) );
	}
}