package nl.papendorp.shoppinglist.integration;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.extern.log4j.Log4j2;
import nl.papendorp.shoppinglist.features.shared.attempt.Attempt;
import nl.papendorp.shoppinglist.main.ShoppingListApp;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.HttpClientBuilder;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Collection;

import static com.google.common.collect.ImmutableSet.copyOf;
import static com.google.common.collect.ImmutableSet.of;
import static java.util.Arrays.asList;
import static nl.papendorp.shoppinglist.features.shared.attempt.Attempt.resultOf;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

@Log4j2
class ShowListIT
{
	private static final String BASE_URI = "http://localhost:8080/";

	private ShoppingListApp shoppingListApp;

	@BeforeEach
	void startServer()
	{
		shoppingListApp = new ShoppingListApp( BASE_URI );
		shoppingListApp.startServer();
	}

	@Test
	void showShoppingList() throws IOException
	{
		Collection< ReceivedItem > expectedItems = of(
				new ReceivedItem( "butter", 1 ),
				new ReceivedItem( "milk", 1 ),
				new ReceivedItem( "eggs", 1 ) );

		addItemsToList( expectedItems );
		Collection< ReceivedItem > actualItems = viewShoppingList();

		assertThat( actualItems, is( expectedItems ) );
	}

	private void addItemsToList( Collection< ReceivedItem > expectedItems )
	{
		expectedItems.forEach( this::addItemToList );
	}

	private void addItemToList( ReceivedItem item )
	{
		String itemUri = String.format( "shoppinglist/add/%s/items/of/%s", item.quantity, item.productName );
		HttpUriRequest postRequest = new HttpPost( BASE_URI + itemUri );
		validResponseFrom( postRequest );
	}

	private Collection< ReceivedItem > viewShoppingList() throws IOException
	{
		HttpUriRequest request = new HttpGet( BASE_URI + "shoppinglist/" );

		HttpResponse httpResponse = validResponseFrom( request );

		ReceivedItem[] receivedItems = parseResponse( httpResponse, ReceivedItem[].class );
		return copyOf( receivedItems );
	}

	private < RESPONSE_TYPE > RESPONSE_TYPE parseResponse( HttpResponse httpResponse,
	                                                       Class< RESPONSE_TYPE > responseType ) throws IOException
	{
		final HttpEntity entity = httpResponse.getEntity();
		final InputStream content = entity.getContent();

		return new ObjectMapper( new JsonFactory() )
				.readerFor( responseType )
				.readValue( content );
	}

	@Test
	void pureJava() throws Exception
	{
		URL url = new URL( BASE_URI + "shoppinglist" );
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setRequestMethod( "GET" );
		conn.setRequestProperty( "Accept", "application/json" );

		assertThat( conn.getResponseCode(), is( 200 ) );

		BufferedReader br = new BufferedReader( new InputStreamReader( (conn.getInputStream()) ) );

		ReceivedItem[] result = new ObjectMapper( new JsonFactory() )
				.readerFor( ReceivedItem[].class )
				.readValue( br );
		conn.disconnect();

		assertThat( asList( result ), containsInAnyOrder(
				new ReceivedItem( "butter", 1 ),
				new ReceivedItem( "milk", 1 ),
				new ReceivedItem( "eggs", 1 ) ) );
	}

	private HttpResponse validResponseFrom( HttpUriRequest request )
	{
		Attempt< HttpResponse > responseAttempt = resultOf( () ->
				HttpClientBuilder.create()
						.build()
						.execute( request ) );

		HttpResponse validResponse = responseAttempt.get();

		assertThat( validResponse.getStatusLine().getStatusCode(),
				is( HttpStatus.SC_OK ) );

		return validResponse;
	}

	@AfterEach
	void stopServer()
	{
		shoppingListApp.shutdownServer();
	}

	@Data
	@AllArgsConstructor
	private static final class ReceivedItem
	{
		private String productName;
		private int quantity;
	}
}
