package nl.papendorp.shoppinglist.shoppinglist.stored;

import nl.papendorp.shoppinglist.items.Item;
import nl.papendorp.shoppinglist.items.SimpleItem;
import nl.papendorp.shoppinglist.shoppinglist.ShoppingList;
import nl.papendorp.shoppinglist.shoppinglist.ShoppingListFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Collection;

import static com.google.common.collect.ImmutableList.copyOf;
import static com.google.common.collect.ImmutableSet.of;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class StoredShoppingListShould
{
	private static final Item ONE = new SimpleItem( "ONE", 1 );
	private static final Item TWO = new SimpleItem( "TWO", 1 );
	private static final Item THREE = new SimpleItem( "THREE", 1 );
	private static final String PRODUCT_NAME = "PRODUCT_NAME";
	private static final String LIST_NAME = "LIST_NAME";

	@Mock
	private ShoppingListRepository mockRepository;

	private ShoppingListFactory shoppingListFactory;

	@BeforeEach
	void initMocks()
	{
		MockitoAnnotations.initMocks( this );
	}

	@BeforeEach
	void withShoppingListFactory()
	{
		shoppingListFactory = new StoredShoppingListFactory( mockRepository, ( item ) -> item );
	}

	@Test
	void retrieveItemsFromRepository()
	{
		withItemsOnMyList( ONE, TWO, THREE );

		Collection< Item > actualItems = whenRetrievingAllItems();

		assertThat( actualItems, containsInAnyOrder( ONE, TWO, THREE ) );
	}

	@Test
	void addItemToRepository()
	{
		withItemsOnMyList( ONE, TWO );

		ShoppingList shoppingList = shoppingListFactory.myListNamed( LIST_NAME );
		shoppingList.addNewItem( THREE );

		verify( mockRepository ).updateList( of( ONE, TWO, THREE ) );
	}

	@Test
	void mergeItemsWithSameProductName()
	{
		withItemsOnMyList( new SimpleItem( PRODUCT_NAME, 2 ) );

		ShoppingList shoppingList = shoppingListFactory.myListNamed( LIST_NAME );
		shoppingList.addNewItem( new SimpleItem( PRODUCT_NAME, 3 ) );

		assertThat( shoppingList.allItems(),
				contains( new SimpleItem( PRODUCT_NAME, 5 ) ) );
	}

	@Test
	void ensureValidResponse()
	{
		when( mockRepository.allItemsOnMyList() ).thenReturn( null );

		Collection< Item > actualItems = whenRetrievingAllItems();

		assertThat( actualItems, is( empty() ) );
	}

	private void withItemsOnMyList( Item... items )
	{
		when( mockRepository.allItemsOnMyList() ).thenReturn( copyOf( items ) );
	}

	private Collection< Item > whenRetrievingAllItems()
	{
		ShoppingList shoppingList = shoppingListFactory.myListNamed( LIST_NAME );
		return shoppingList.allItems();
	}
}
