package nl.papendorp.shoppinglist.shoppinglist.stored;

import nl.papendorp.shoppinglist.items.Item;
import org.junit.Before;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static com.google.common.collect.ImmutableSet.of;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;

class InMemoryRepositoryShould
{

	private InMemoryRepository repository;

	@BeforeEach
	void setUp() throws Exception
	{
		repository = new InMemoryRepository();
	}

	@Test
	void simplyMaintainGivenCollection()
	{

		Item one = mock( Item.class );
		Item two = mock( Item.class );
		Item three = mock( Item.class );

		repository.updateList( of( one, two, three ) );

		assertThat( repository.allItemsOnMyList(), containsInAnyOrder( one, two, three ) );
	}

	@Test
	void startWithEmptyList()
	{
		assertThat( repository.allItemsOnMyList(), is( empty() ) );
	}
}