Feature: Show my shopping list

  Background:
    Given that I have a list called groceries

  Scenario: An empty list contains no items
    Given that my groceries list contains
      | productName | quantity |
    When I look at my groceries list
    Then I see a list containing
      | productName | quantity |

  Scenario: Looking at a list with multiple items
    Given that my groceries list contains
      | productName | quantity |
      | milk        | 1        |
      | butter      | 1        |
      | eggs        | 1        |
    When I look at my groceries list
    Then I see a list containing
      | productName | quantity |
      | milk        | 1        |
      | butter      | 1        |
      | eggs        | 1        |

  Scenario: Looking at a list with multiple items of the same type
    Given that my groceries list contains
      | productName | quantity |
      | milk        | 3        |
    When I look at my groceries list
    Then I see a list containing
      | productName | quantity |
      | milk        | 3        |
