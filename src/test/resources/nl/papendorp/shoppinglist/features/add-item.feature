Feature: Adding new items to the list

  Background:
    Given that I have a list called groceries
    And that my groceries list contains
      | productName | quantity |
      | milk        | 1        |
      | butter      | 1        |
      | eggs        | 1        |

  Scenario: Adding an new item to the list includes it
    When I add 1 items of cheese to my groceries list
    Then my groceries list contains
      | productName | quantity |
      | milk        | 1        |
      | butter      | 1        |
      | eggs        | 1        |
      | cheese      | 1        |

  Scenario: Adding an existing item to the list sums the quantities
    When I add 2 items of butter to my groceries list
    Then my groceries list contains
      | productName | quantity |
      | milk        | 1        |
      | butter      | 3        |
      | eggs        | 1        |

  Scenario: Adding items with a negative quantity is not allowed
    When I add -1 items of cheese to my groceries list
    Then an invalid quantity error occurs
    And my groceries list contains
      | productName | quantity |
      | milk        | 1        |
      | butter      | 1        |
      | eggs        | 1        |

  Scenario: Adding items without a product name is not allowed
    When I add 1 items of     to my groceries list
    Then an invalid product name error occurs
    And my groceries list contains
      | productName | quantity |
      | milk        | 1        |
      | butter      | 1        |
      | eggs        | 1        |
