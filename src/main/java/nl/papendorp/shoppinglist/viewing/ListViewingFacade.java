package nl.papendorp.shoppinglist.viewing;

import nl.papendorp.shoppinglist.items.Item;
import nl.papendorp.shoppinglist.items.ItemConverter;
import nl.papendorp.shoppinglist.shoppinglist.ShoppingList;
import nl.papendorp.shoppinglist.shoppinglist.ShoppingListFactory;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.Collection;

@Named
public class ListViewingFacade implements ListViewingService
{
	private final ShoppingListFactory shoppingListFactory;
	private final ItemConverter itemConverter;

	@Inject
	public ListViewingFacade( ShoppingListFactory shoppingListFactory, ItemConverter itemConverter )
	{
		this.shoppingListFactory = shoppingListFactory;
		this.itemConverter = itemConverter;
	}

	@Override
	public Collection< Item > viewMyListNamed( String listName )
	{
		ShoppingList myList = shoppingListFactory.myListNamed( listName );
		return myList.allItems();
	}

	@Override
	public void addItemToMyListNamed( String listName, Item item )
	{
		Item convertedItem = itemConverter.from( item );
		ShoppingList shoppingList = shoppingListFactory.myListNamed( listName );
		shoppingList.addNewItem( convertedItem );
	}
}
