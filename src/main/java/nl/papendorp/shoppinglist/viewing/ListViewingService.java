package nl.papendorp.shoppinglist.viewing;

import nl.papendorp.shoppinglist.items.Item;

import java.util.Collection;

public interface ListViewingService
{
	Collection< Item > viewMyListNamed( String listName );

	void addItemToMyListNamed( String listName, Item item );
}
