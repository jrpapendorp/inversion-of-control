package nl.papendorp.shoppinglist.configuration;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan( { "nl.papendorp.shoppinglist" } )
public class ShoppingListSpringConfiguration
{
}
