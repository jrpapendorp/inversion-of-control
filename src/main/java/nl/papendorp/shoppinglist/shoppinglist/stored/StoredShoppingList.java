package nl.papendorp.shoppinglist.shoppinglist.stored;

import nl.papendorp.shoppinglist.items.Item;
import nl.papendorp.shoppinglist.items.ItemConverter;
import nl.papendorp.shoppinglist.shoppinglist.ShoppingList;

import java.util.Collection;
import java.util.Optional;
import java.util.TreeSet;

import static java.util.Collections.emptySet;
import static java.util.Comparator.comparing;
import static java.util.Optional.ofNullable;
import static java.util.stream.Collectors.toCollection;

public class StoredShoppingList implements ShoppingList
{
	private final ShoppingListRepository repository;
	private final ItemConverter itemConverter;

	private final Collection< Item > itemsOnList;

	StoredShoppingList( ShoppingListRepository repository, ItemConverter itemConverter )
	{
		this.repository = repository;
		this.itemConverter = itemConverter;

		this.itemsOnList = loadItems();
	}

	private Collection< Item > loadItems()
	{
		return ofNullable( repository.allItemsOnMyList() )
				.orElse( emptySet() )
				.stream()
				.map( itemConverter::from )
				.collect( toCollection( () -> new TreeSet<>( comparing( Item::productName ) ) ) );
	}

	@Override
	public Collection< Item > allItems()
	{
		return itemsOnList;
	}

	@Override
	public void addNewItem( Item newItem )
	{
		Optional< Item > possibleOldItem = findItem( newItem );

		if( possibleOldItem.isPresent() )
		{
			mergeItems( newItem, possibleOldItem.get() );
		}
		else
		{
			itemsOnList.add( newItem );
		}

		repository.updateList( itemsOnList );
	}

	private Optional< Item > findItem( Item newItem )
	{
		return itemsOnList
				.stream()
				.filter( item -> item.productName().equals( newItem.productName() ) )
				.findFirst();
	}

	private void mergeItems( Item newItem, Item oldItem )
	{
		Item mergedItem = oldItem.addQuantity( newItem.quantity() );
		itemsOnList.remove( oldItem );
		itemsOnList.add( mergedItem );
	}
}
