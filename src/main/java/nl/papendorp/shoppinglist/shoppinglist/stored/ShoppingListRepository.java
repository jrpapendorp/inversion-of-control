package nl.papendorp.shoppinglist.shoppinglist.stored;

import nl.papendorp.shoppinglist.items.Item;

import java.util.Collection;

public interface ShoppingListRepository
{
	Collection< Item > allItemsOnMyList();

	void updateList( Collection< Item > updatedList );
}
