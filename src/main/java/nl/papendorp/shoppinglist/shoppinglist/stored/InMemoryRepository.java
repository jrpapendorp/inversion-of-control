package nl.papendorp.shoppinglist.shoppinglist.stored;

import nl.papendorp.shoppinglist.items.Item;

import javax.inject.Named;
import java.util.Collection;

import static com.google.common.collect.ImmutableSet.of;

@Named
public class InMemoryRepository implements ShoppingListRepository
{
	private Collection< Item > items = of();

	@Override
	public Collection< Item > allItemsOnMyList()
	{
		return items;
	}

	@Override
	public void updateList( Collection< Item > updatedList )
	{
		items = updatedList;
	}
}
