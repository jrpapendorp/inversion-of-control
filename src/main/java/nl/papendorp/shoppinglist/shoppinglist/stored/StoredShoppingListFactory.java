package nl.papendorp.shoppinglist.shoppinglist.stored;

import nl.papendorp.shoppinglist.items.ItemConverter;
import nl.papendorp.shoppinglist.shoppinglist.ShoppingList;
import nl.papendorp.shoppinglist.shoppinglist.ShoppingListFactory;

import javax.inject.Inject;
import javax.inject.Named;

@Named
public class StoredShoppingListFactory implements ShoppingListFactory
{
	private final ShoppingListRepository repository;
	private final ItemConverter itemConverter;

	@Inject
	public StoredShoppingListFactory( ShoppingListRepository repository, ItemConverter itemConverter )
	{
		this.repository = repository;
		this.itemConverter = itemConverter;
	}

	@Override
	public ShoppingList myListNamed( String listName )
	{
		return new StoredShoppingList( repository, itemConverter );
	}
}
