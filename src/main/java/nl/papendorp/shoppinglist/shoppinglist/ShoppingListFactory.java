package nl.papendorp.shoppinglist.shoppinglist;

public interface ShoppingListFactory
{
	ShoppingList myListNamed( String listName );
}
