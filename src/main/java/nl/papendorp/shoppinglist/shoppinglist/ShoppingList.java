package nl.papendorp.shoppinglist.shoppinglist;

import nl.papendorp.shoppinglist.items.Item;

import java.util.Collection;

public interface ShoppingList
{
	Collection< Item > allItems();

	void addNewItem( Item item );
}
