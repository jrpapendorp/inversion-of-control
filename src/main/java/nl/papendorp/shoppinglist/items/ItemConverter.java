package nl.papendorp.shoppinglist.items;

public interface ItemConverter
{
	Item from( Item other );
}
