package nl.papendorp.shoppinglist.items;

public interface ItemFactory extends ItemConverter
{
	ItemBuilder buildItem();

	default Item from( Item other )
	{
		return buildItem()
				.withProductName( other.productName() )
				.withQuantity( other.quantity() );
	}


	@FunctionalInterface
	interface ItemBuilder
	{
		PartialItem withProductName( String name );
	}

	@FunctionalInterface
	interface PartialItem
	{
		Item withQuantity( int quantity );
	}
}
