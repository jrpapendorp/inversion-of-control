package nl.papendorp.shoppinglist.items.basic;


import lombok.EqualsAndHashCode;
import lombok.ToString;

@EqualsAndHashCode
@ToString
class Quantity
{
	final int value;

	Quantity( int value )
	{
		this.value = verifyPositive( value );
	}

	private int verifyPositive( int quantity )
	{
		if( quantity < 1 )
		{
			throw new IllegalArgumentException( "Illegal quantity: " + quantity );
		}
		return quantity;
	}

	Quantity add( int addend )
	{
		return new Quantity( value + addend );
	}
}
