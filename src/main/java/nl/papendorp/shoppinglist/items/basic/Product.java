package nl.papendorp.shoppinglist.items.basic;

import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.apache.commons.lang3.StringUtils;

@EqualsAndHashCode
@ToString
class Product
{
	final String name;

	Product( String name )
	{
		this.name = ensurePresent( name );
	}

	private String ensurePresent( String productName )
	{
		if( StringUtils.isBlank( productName ) )
		{
			throw new IllegalArgumentException( "Missing product name" );
		}
		return productName;
	}
}
