package nl.papendorp.shoppinglist.items.basic;

import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;
import lombok.ToString;
import nl.papendorp.shoppinglist.items.Item;

@EqualsAndHashCode
@RequiredArgsConstructor
@ToString
class BasicItem implements Item
{
	private final Product product;
	private final Quantity quantity;

	@Override
	public String productName()
	{
		return product.name;
	}

	@Override
	public int quantity()
	{
		return quantity.value;
	}

	@Override
	public Item addQuantity( int quantity )
	{
		return new BasicItem( product, this.quantity.add( quantity ) );
	}
}
