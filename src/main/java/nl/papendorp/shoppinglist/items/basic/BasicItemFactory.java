package nl.papendorp.shoppinglist.items.basic;

import nl.papendorp.shoppinglist.items.ItemFactory;

import javax.inject.Named;

@Named
class BasicItemFactory implements ItemFactory
{
	@Override
	public ItemBuilder buildItem()
	{
		return ( productName ) -> ( quantity ) ->
				new BasicItem(
						new Product( productName ),
						new Quantity( quantity )
				);
	}
}
