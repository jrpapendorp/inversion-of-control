package nl.papendorp.shoppinglist.items;

public interface Item
{
	String productName();

	int quantity();

	Item addQuantity( int quantity );
}
