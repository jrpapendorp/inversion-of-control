package nl.papendorp.shoppinglist.main;

import lombok.extern.log4j.Log4j2;
import nl.papendorp.shoppinglist.configuration.ShoppingListSpringConfiguration;
import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.hk2.api.ServiceLocator;
import org.glassfish.hk2.api.ServiceLocatorFactory;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.glassfish.jersey.server.ResourceConfig;
import org.jvnet.hk2.spring.bridge.api.SpringBridge;
import org.jvnet.hk2.spring.bridge.api.SpringIntoHK2Bridge;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.io.IOException;
import java.net.URI;

@Log4j2
public class ShoppingListApp
{
	private final String uri;
	private HttpServer httpServer;

	public ShoppingListApp( String uri )
	{
		this.uri = uri;
	}

	public static void main( String[] args ) throws IOException
	{
		final String BASE_URI = "http://localhost:8080/";

		ShoppingListApp shoppingListApp = new ShoppingListApp(BASE_URI);
		shoppingListApp.startServer();

		reportServerStart(BASE_URI);

		shutdownOnKeyStroke( shoppingListApp );
	}

	private static void reportServerStart(String uri)
	{
		log.info( "Jersey app started" );
		log.info( String.format( "WADL available at %sapplication.wadl", uri ) );
		log.info("Hit enter to stop it..." );
	}

	private static void shutdownOnKeyStroke( ShoppingListApp shoppingListApp ) throws IOException
	{
		System.in.read();
		shoppingListApp.shutdownServer();
	}

	public void startServer()
	{
		ServiceLocator serviceLocator = initializeDependencyInjection();
		createGrizzlyServer( serviceLocator );
	}

	private ServiceLocator initializeDependencyInjection()
	{
		ServiceLocator serviceLocator = createHK2ServiceLocator();
		integrateSpringIntoHK2( serviceLocator );
		return serviceLocator;
	}

	private ServiceLocator createHK2ServiceLocator()
	{
		ServiceLocatorFactory locatorFactory = ServiceLocatorFactory.getInstance();
		return locatorFactory.create( "TestLocator" );
	}

	private void integrateSpringIntoHK2( ServiceLocator serviceLocator )
	{
		SpringIntoHK2Bridge springBridge = createSpringIntoHK2Bridge( serviceLocator );
		BeanFactory springContext = createSpringContext();
		springBridge.bridgeSpringBeanFactory( springContext );
	}

	private SpringIntoHK2Bridge createSpringIntoHK2Bridge( ServiceLocator serviceLocator )
	{
		SpringBridge.getSpringBridge().initializeSpringBridge( serviceLocator );
		return serviceLocator.getService( SpringIntoHK2Bridge.class );
	}

	private BeanFactory createSpringContext()
	{
		return new AnnotationConfigApplicationContext( ShoppingListSpringConfiguration.class );
	}

	private void createGrizzlyServer( ServiceLocator serviceLocator )
	{
		final ResourceConfig rc = new ResourceConfig()
				.packages( "nl.papendorp.shoppinglist.resources" );

		httpServer = GrizzlyHttpServerFactory.createHttpServer(
				URI.create( uri ),
				rc,
				serviceLocator );
	}

	public void shutdownServer()
	{
		httpServer.shutdown();
	}
}
