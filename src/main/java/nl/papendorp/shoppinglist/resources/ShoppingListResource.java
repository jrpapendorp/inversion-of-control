package nl.papendorp.shoppinglist.resources;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import nl.papendorp.shoppinglist.items.Item;
import nl.papendorp.shoppinglist.viewing.ListViewingService;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import java.util.Collection;

import static java.util.stream.Collectors.toSet;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

@Singleton
@Path( "shoppinglist" )
@Log4j2
public class ShoppingListResource
{
	private final ListViewingService listViewingService;

	@Inject
	public ShoppingListResource( ListViewingService listViewingService )
	{
		this.listViewingService = listViewingService;
	}

	@GET
	@Produces( APPLICATION_JSON )
	public Collection< ItemResponseWrapper > viewMyListAsJSON()
	{
		log.info( "viewing list" );
		return listViewingService.viewMyListNamed( "LIST_NAME" )
				.stream()
				.map( ItemResponseWrapper::new )
				.collect( toSet() );
	}

	@POST
	@Path( "add/{quantity}/items/of/{productName}" )
	public Response addItemToList(
			@PathParam( "quantity" ) int quantity,
			@PathParam( "productName" ) String productName )
	{
		log.info( "addItemToList: quantity {}, product name {}", quantity, productName );
		ItemResponseWrapper item = new ItemResponseWrapper( productName, quantity );
		listViewingService.addItemToMyListNamed( "LIST_NAME", item );

		return Response
				.status(200)
				.entity("Item added " + item)
				.build();
	}

	@Data
	@RequiredArgsConstructor
	private static final class ItemResponseWrapper implements Item
	{
		private final String productName;
		private final int quantity;

		ItemResponseWrapper( Item item )
		{
			this( item.productName(), item.quantity() );
		}

		@Override
		public String productName()
		{
			return productName;
		}

		@Override
		public int quantity()
		{
			return quantity;
		}

		@Override
		public Item addQuantity( int quantity )
		{
			throw new UnsupportedOperationException();
		}
	}
}
